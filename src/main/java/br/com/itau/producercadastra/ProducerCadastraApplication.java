package br.com.itau.producercadastra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProducerCadastraApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProducerCadastraApplication.class, args);
	}

}
