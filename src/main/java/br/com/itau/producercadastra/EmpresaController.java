package br.com.itau.producercadastra;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/empresa")
public class EmpresaController {

    @Autowired
    private EmpresaProducer empresaProducer;

    @PostMapping
    public void create(@RequestBody Empresa empresa) {
        empresaProducer.enviarAoKafka(empresa);
    }

}
